package com.epam.task11.multythreading.view;

import com.epam.task11.multythreading.controller.Executable;
import com.epam.task11.multythreading.controller.ShopController;
import com.epam.task11.multythreading.utilities.ConsoleReader;
import com.epam.task11.multythreading.utilities.Constants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements Printable {

  private Logger logger = LogManager.getLogger(ConsoleView.class);
  private Map<Integer, Executable> mainMenu;
  private ConsoleReader consoleReader;

  public ConsoleView() {
    consoleReader = new ConsoleReader();
    mainMenu = new HashMap<>();
    mainMenu.put(0, new ShopController());
  }

  private void printList(List<String> collection) {
    collection.forEach(s -> logger.info(s));
  }

  @Override
  public void print() {
    int menuItem;
    while (true) {
      logger.info(Constants.MAIN_MENU);
      menuItem = consoleReader.enterMenuItem();
      if (menuItem == Constants.EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      logger.info("-------------------------------------------------------------------------");
      printList(mainMenu.get(menuItem).execute());
      logger.info("-------------------------------------------------------------------------");
    }
  }
}
