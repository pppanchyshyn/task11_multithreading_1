package com.epam.task11.multythreading.view;

public interface Printable {

  void print();
}
