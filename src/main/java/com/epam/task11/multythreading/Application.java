package com.epam.task11.multythreading;

import com.epam.task11.multythreading.view.ConsoleView;

public class Application {

  public static void main(String[] args) {
    new ConsoleView().print();
  }
}
