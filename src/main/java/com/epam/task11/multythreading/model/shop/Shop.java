package com.epam.task11.multythreading.model.shop;

import com.epam.task11.multythreading.utilities.Constants;
import java.util.ArrayList;
import java.util.List;

public class Shop {

  private int product = 0;
  private List<String> register;

  public Shop() {
    register = new ArrayList<>();
  }

  public List<String> getRegister() {
    return register;
  }

  public synchronized void get() throws InterruptedException {
    while (product < 1) {
      wait();
    }
    product--;
    notify();
    register.add(String.format("Buyer bought one item. %d items in stock", product));
  }

  public synchronized void put() throws InterruptedException {
    while (product >= Constants.MAX_ITEM_AMOUNT) {
      wait();
    }
    product++;
    notify();
    register.add(String.format("Manufacturer added one item. %d items in stock", product));
  }
}
