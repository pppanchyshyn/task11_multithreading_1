package com.epam.task11.multythreading.model.shop;

import com.epam.task11.multythreading.utilities.Constants;

public class Buyer implements Runnable {

  private Shop shop;

  public Buyer(Shop shop) {
    this.shop = shop;
  }

  public void run() {
    for (int i = 1; i < Constants.TRANSACTIONS_AMOUNT; i++) {
      try {
        shop.get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
