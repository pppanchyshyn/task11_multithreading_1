package com.epam.task11.multythreading.utilities;

public class Constants {

  public final static int EXIT_MENU_ITEM = 1;
  public final static int MAX_ITEM_AMOUNT = 1;
  public final static int TRANSACTIONS_AMOUNT = 10;
  public final static String MAIN_MENU = "To print \"ping-pong\" program demo press 0\n"
      + "To exit program press 1\n"
      + "\nMake your choice: ";

  private Constants() {
  }
}
