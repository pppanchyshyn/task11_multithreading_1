package com.epam.task11.multythreading.controller;

import com.epam.task11.multythreading.utilities.ConsoleReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class Executor implements Executable {

  Logger logger = LogManager.getLogger(Executor.class);
  List<String> results;
  ConsoleReader consoleReader;

  Executor() {
    results = new ArrayList<>();
    consoleReader = new ConsoleReader();
  }
}
