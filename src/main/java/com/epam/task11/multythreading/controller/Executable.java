package com.epam.task11.multythreading.controller;

import java.util.List;

public interface Executable {

  List<String> execute();
}
