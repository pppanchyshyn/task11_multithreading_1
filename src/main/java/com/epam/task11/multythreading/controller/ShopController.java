package com.epam.task11.multythreading.controller;

import com.epam.task11.multythreading.model.shop.Buyer;
import com.epam.task11.multythreading.model.shop.Manufacturer;
import com.epam.task11.multythreading.model.shop.Shop;
import java.util.List;

public class ShopController extends Executor {

  private Shop shop;
  private Manufacturer manufacturer;
  private Buyer buyer;

  public ShopController() {
    shop = new Shop();
    manufacturer = new Manufacturer(shop);
    buyer = new Buyer(shop);
  }

  @Override
  public List<String> execute() {
    results = shop.getRegister();
    new Thread(manufacturer).start();
    new Thread(buyer).start();
    return results;
  }
}
